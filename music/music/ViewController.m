//
//  ViewController.m
//  music
//
//  Created by Clicklabs 104 on 10/9/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *music;
@property (weak, nonatomic) IBOutlet UIButton *agnee;

@property (weak, nonatomic) IBOutlet UIButton *bhula;
@end

@implementation ViewController
@synthesize music;
@synthesize agnee;
@synthesize bhula;
AVAudioPlayer *audioPlayer;
AVAudioPlayer *audioPlayer2;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)agnee:(id)sender {
    NSString *path = [[NSBundle mainBundle]
                      pathForResource:@"agnee" ofType:@"mp3"];
    audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:
                   [NSURL fileURLWithPath:path] error:NULL];
    [audioPlayer play];
    
}
- (IBAction)bhula:(id)sender {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"bhuladena" ofType:@"mp3"];
    audioPlayer2 = [[AVAudioPlayer alloc]initWithContentsOfURL: [NSURL fileURLWithPath:path] error:NULL];
    [audioPlayer2 play];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
